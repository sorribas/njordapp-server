var Client = require('ftp');
var http = require('http');
var split = require('split2');

var cache = {};
var server = http.createServer(function (req, res) {
  if (cache.time && Date.now() - cache.time < (60000 * 5)) return res.end(JSON.stringify(cache.data));
  var client = new Client();
  client.on('ready', onready)
  client.connect({
    host: '194.239.2.174',
    user: 'ftp000148',
    password: '?4xesufevuyEta'
  });

  function onready () {
    client.list('onlinedata', onlist);
  }

  function onlist (err, list) {
    if (err) return error(err, res);
    client.get('onlinedata/' + list[list.length - 1].name, onget);
  }

  function onget (err, stream) {
    if (err) return error(err, res);
    var line;
    var lineStream = stream.pipe(split());
    lineStream.on('data', function (data) {
      line = data;
    });

    lineStream.on('end', function () {
      var vars = line.split(';')
      var fossil = Number(vars[1]) + Number(vars[2]) + Number(vars[3]) + Number(vars[4]);
      var renewable = Number(vars[5]) + Number(vars[6]) + Number(vars[19]) + Number(vars[20]);
      var data = {fossil: fossil, renewable: renewable}
      cache = {data: data, time: Date.now()};
      res.end(JSON.stringify(data));
    });
  }
});

server.listen(process.env.PORT || 3200, function () {
  console.log('listening on 3200');
});

function error (err) {
  
}
